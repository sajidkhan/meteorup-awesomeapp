module.exports = {
  servers: {
    one: {
      // TODO: set host address, username, and authentication method
      host: '143.198.211.163',
      username: 'root',
      pem: '~/meteor_key',
      // or neither for authenticate from ssh-agent
      opts: {
        port: 22
      },
    },
  },

  app: {
    // TODO: change app name and path
    name: 'awesomeapp',
    path: '../',
    type: 'meteor',
    docker: {
      image: 'zodern/meteor',
      buildInstructions: [
        'USER 0:0',
        'RUN apt-get update && apt-get install -y imagemagick',
      ],
      
    },
    servers: {
      one: {},
    },

    buildOptions: {
      serverOnly: true,
      buildLocation: '/tmp',
    },

    env: {
      // TODO: Change to your app's url
      // If you are using ssl, it needs to start with https://
      ROOT_URL: 'http://meteor.t99ltd.info',
      // change the url with your external MongoDB URL
      // MONGO_URL: 'mongodb://mongodb/meteor', 
      MONGO_URL: 'mongodb+srv://doadmin:792O345VHE6MlZG1@db-mongodb-sgp1-31708-4420b383.mongo.ondigitalocean.com/admin?tls=true&authSource=admin',
      
    },

    // Show progress bar while uploading bundle to server
    // You might need to disable it on CI servers
    enableUploadProgressBar: true
  },
  /**
   * If you want to use built in MongoDB please uncomment following object 
   * 
   */
  //  mongo: {
  //   version: '3.6',

  //   servers: {
  //     one: {}
  //   }
  // },

  proxy: {
    domains: 'meteor.t99ltd.info',
    servers: {
      one: {}
    },
  },
};
