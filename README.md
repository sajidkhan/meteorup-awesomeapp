# meteorup-awesomeapp

meteorup-awesomeapp is meant to help you run your Meteor app with a reverse proxy of Nginx at the front end in docker containers.

## Notice
- Ensure Meteor is installed.
- Ensure an Ubuntu server with public IP.
- Ensure **ssh key** authentication to server.
- Ensure a domain name for Nginx server.
- 

## How to Use


###  1. Build Your Meteor App

Issue this command

~~~shell
meteor create awesomeapp
~~~

###  2. mup installation

Issue this command

~~~shell
npm install -g mup
~~~
mup should be installed on the computer you are deploying from. Node 8 or newer is required.

### 3. Creating a Meteor Up Project
~~~shell
cd awesomeapp
mkdir .deploy
cd .deploy
mup init
~~~
This will create two files in your Meteor Up project directory:

- mup.js - Meteor Up configuration file
- settings.json - Settings for Meteor’s settings API

### 4. Modify mup.js config file according to your requirement

~~~javascript
module.exports = {
  servers: {
    one: {
      // TODO: set host address, username, and authentication method
      host: '143.198.211.163',
      username: 'root',
      pem: '~/meteor_key',
      // or neither for authenticate from ssh-agent
      opts: {
        port: 22
      },
    },
  },

  app: {
    // TODO: change app name and path
    name: 'awesomeapp',
    path: '../',
    type: 'meteor',
    docker: {
      image: 'zodern/meteor',
      buildInstructions: [
        'USER 0:0',
        'RUN apt-get update && apt-get install -y imagemagick',
      ],
      
    },
    servers: {
      one: {},
    },

    buildOptions: {
      serverOnly: true,
      buildLocation: '/tmp',
    },

    env: {
      // TODO: Change to your app's url
      // If you are using ssl, it needs to start with https://
      ROOT_URL: 'http://meteor.t99ltd.info',
      // change the url with your external MongoDB URL
      // MONGO_URL: 'mongodb://mongodb/meteor', 
      MONGO_URL: 'mongodb+srv://doadmin:792O345VHE6MlZG1@db-mongodb-sgp1-31708-4420b383.mongo.ondigitalocean.com/admin?tls=true&authSource=admin',
      
    },

    // Show progress bar while uploading bundle to server
    // You might need to disable it on CI servers
    enableUploadProgressBar: true
  },
  /**
   * If you want to use built in MongoDB please uncomment following object 
   * 
   */
  //  mongo: {
  //   version: '3.6',

  //   servers: {
  //     one: {}
  //   }
  // },

  proxy: {
    domains: 'meteor.t99ltd.info',
    servers: {
      one: {}
    },
  },
};

~~~
### 5. Setting Up a Server

~~~shell
mup setup
~~~

Running this locally will set up the remote servers you have specified in your config. It will take around 2-5 minutes depending on the server’s performance and network availability.

It is safe to run mup setup multiple times if needed. After making changes to custom SSL certificates, MongoDB, or servers in your config, you need to run mup setup for the changes to take effect.

### 6. Deploying an App

~~~shell
mup deploy --verbose
~~~

This will bundle the Meteor project locally and deploy it to the remote server(s). The bundling process is the same as what meteor deploy does.

After succesfully deployment check applications status using following command:-

~~~shell
mup status
~~~
If everthing deployed correctly then you could see an output like below:-
```
=> Servers
  - 143.198.211.163: Ubuntu 20.04
  - 188.166.227.186: Ubuntu 20.04

=> Docker Status
 - 143.198.211.163: 20.10.21 Running
 - 188.166.227.186: 20.10.21 Running

=> Meteor Status - awesomeapp
  - 143.198.211.163: running
    Created at 2022-10-28T21:33:09.354768003Z
    Restarted 0 times
    ENV:
      - ROOT_URL=http://meteor.t99ltd.info
      - MONGO_URL=mongodb://mongodb:27017/awesomeapp?replicaSet=meteor
      - VIRTUAL_HOST=meteor.t99ltd.info
      - HTTPS_METHOD=noredirect
      - VIRTUAL_PORT=3000
      - HTTP_FORWARDED_COUNT=1
      - METEOR_SETTINGS={"public":{}}
      - PORT=3000
      - PATH=/home/app/.onbuild-node/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
      - NODE_PATH=/home/app/.onbuild-node/lib/node_modules
    Exposed Ports:
      - 3000/tcp
    App available through reverse proxy
      - Available in app's docker container: true

=> Mongo Status
  running on server 143.198.211.163
  Restarted 0 times
  Running since 2022-10-28T21:32:29.193175744Z
  Version 3.6.23
  Connections: 3
  Storage Engine: wiredTiger

=> Reverse Proxy Status
 - 143.198.211.163:
   - NGINX:
     - Status: running
     - Ports:
       - HTTPS: 443
       - HTTP: 80
   - Let's Encrypt
     - Status: running
```
Now browse the application using you domain:-

- http://meteor.t99ltd.info

### 7. Other Utility Commands

- ```mup reconfig``` - reconfigures app with new environment variables, Meteor settings, and it updates the start script. This is also the last step of mup deploy.
- ```mup stop``` - stop the app
- ```mup start``` - start the app
- ```mup restart``` - restart the app
- ```mup logs [-f --tail=50]``` - view the app’s logs. Supports all of the flags from docker logs.